/**
 * --------------------------------------------------------------------
 * Tooltip plugin for the jQuery-Plugin "Visualize"
 * Tolltip by IraĂŞ Carvalho, irae@irae.pro.br, http://irae.pro.br/en/
 * Copyright (c) 2010 IraĂŞ Carvalho
 * Dual licensed under the MIT (filamentgroup.com/examples/mit-license.txt) and GPL (filamentgroup.com/examples/gpl-license.txt) licenses.
 * 	
 * Visualize plugin by Scott Jehl, scott@filamentgroup.com
 * Copyright (c) 2009 Filament Group, http://www.filamentgroup.com
 *
 * --------------------------------------------------------------------
 */
(function(e){e.visualizePlugins.push(function(t,n){var r=e.extend({tooltip:!1,tooltipalign:"auto",tooltipvalign:"top",tooltipclass:"visualize-tooltip",tooltiphtml:function(e){if(t.multiHover){var n="";for(var r=0;r<e.point.length;r++)n+="<p>"+e.point[r].value+" - "+e.point[r].yLabels[0]+"</p>";return n}return"<p>"+e.point.value+" - "+e.point.yLabels[0]+"</p>"},delay:!1},t);if(!r.tooltip)return;var i=e(this),s=i.next(),o=s.find(".visualize-scroller"),u=o.width(),f=s.find(".visualize-interaction-tracker");f.css({backgroundColor:"white",opacity:0,zIndex:100});var l=e('<div class="'+r.tooltipclass+'"/>').css({position:"absolute",display:"none",zIndex:90}).insertAfter(o.find("canvas")),c=!0;typeof G_vmlCanvasManager!="undefined"&&(o.css({position:"absolute"}),f.css({marginTop:"-"+r.height+"px"})),i.bind("vizualizeOver",function(t,n){if(n.canvasContain.get(0)!=s.get(0))return;if(r.multiHover)var i=n.point[0].canvasCords;else var i=n.point.canvasCords;var f,p,d,v,m,y,b=Math.round(i[0]+n.tableData.zeroLocX),w=Math.round(i[1]+n.tableData.zeroLocY);r.tooltipalign=="left"||r.tooltipalign=="auto"&&b-o.scrollLeft()<=u/2?(!e.browser.msie||e.browser.version!=7&&e.browser.version!=6?c=!0:c=!1,f=b-(c?o.scrollLeft():0)+"px",p="",clasAdd="tooltipleft",v="tooltipright"):(e.browser.msie&&e.browser.version==7?c=!1:c=!0,f="",p=Math.abs(b-r.width)-(r.width-(c?o.scrollLeft():0)-u)+"px",clasAdd="tooltipright",v="tooltipleft"),l.addClass(clasAdd).removeClass(v).html(r.tooltiphtml(n)).css({display:"block",top:w+"px",left:f,right:p})}),i.bind("vizualizeOut",function(e,t){l.css({display:"none"})})})})(jQuery);