class ResultsController < ApplicationController
	def index
		if params['contact'] == nil
			return redirect_to search_url
		end
		@contacts = Contact.all
		if params['contact']['district'].empty?
			@json = Contact.where('service = ?',params['contact']['service']).to_gmaps4rails
		elsif params['contact']['service'].empty?
			@json = Contact.where('district = ?',params['contact']['district']).to_gmaps4rails
		else
			@json = Contact.where('district = ? and service = ?',params['contact']['district'],params['contact']['service']).to_gmaps4rails
		end
		respond_to do |format|
			format.html # index.html.erb
			format.json { render json: @contacts }
		end
	end
end