class District < ActiveRecord::Base
	attr_accessible :name,:longitude_late,:latitude_late,:latitude,:longitude

	acts_as_gmappable


	def gmaps4rails_address
		address
	end
	def gmaps4rails_infowindow
		"#{self.name}"
	end
	def gmaps4rails_sidebar
		"<span class='label btn-info'>#{name}</span>"
	end

	def self.all_to_array
		array = District.all
		str = ""
		array.each { |e|
			str += '"'+e.name+'"'
			if e != array.last
				str += ","
			end
		}
		return str
	end

	
	before_save :fixed_address
	before_update :fixed_address
	after_save :fixed_address
	after_update :fixed_address
	private
	def fixed_address
		self.gmaps = true
		self.latitude = self.latitude_late
		self.longitude = self.longitude_late
	end
end