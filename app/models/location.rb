class Location < ActiveRecord::Base
  attr_accessible :address, :gmaps, :latitude, :longitude
  acts_as_gmappable

	def gmaps4rails_address
		address
	end
	
	# def gmaps4rails_marker_picture
	# 	{
	# 		"picture" => "/assets/img/shopping.png",
	# 		"width" => 25,
	# 		"height" => 25,
	# 		"marker_anchor" => [ 5, 10],
	# 		"shadow_width" => "25",
	# 		"shadow_height" => "25",
	# 		"shadow_anchor" => [5, 3000],
	# 	}
	# end
end
