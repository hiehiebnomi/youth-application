class Contact < ActiveRecord::Base
	attr_accessible :address, :city, :country, :description, :service, :district, :img_default, :name, :province,:ward,:longitude_late,:latitude_late

	acts_as_gmappable


	def gmaps4rails_address
		address
	end
	def gmaps4rails_infowindow
		info = '/contacts/' + id.to_s
		book = 'B&#7841;n &#273;&#227; &#273;&#7863;t ch&#7895; th&#224;nh c&#244;ng [ ',name,' ]'
		"<span class='label btn-info btn-small'>#{name}</span> <a href='#{info}' class='label btn-success btn-small'>Xem</a> <a href='javascript:alert(#{book});' class='label btn-danger'>&#272;&#7863;t Ch&#7895;</a><br/>#{address} / #{province} / #{district}"
	end
	def gmaps4rails_sidebar
		"<span class='label btn-info btn-mini'>#{name}<span>"
	end

	before_save :fixed_address
	before_update :fixed_address

	after_save :fixed_address
	after_update :fixed_address
	private
	def fixed_address
		self.gmaps = true
		self.latitude = self.latitude_late
		self.longitude = self.longitude_late
	end
end
