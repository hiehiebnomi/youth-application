class Service < ActiveRecord::Base
  attr_accessible :name
  
  	def self.all_to_array
		array = Service.all
		str = ""
		array.each { |e|
			str += '"'+e.name+'"'
			if e != array.last
				str += ","
			end
		}
		return str
	end
end
