require 'test_helper'

class AuthControllerTest < ActionController::TestCase
  test "should get facebook" do
    get :facebook
    assert_response :success
  end

  test "should get close" do
    get :close
    assert_response :success
  end

  test "should get sign_out" do
    get :sign_out
    assert_response :success
  end

  test "should get callback" do
    get :callback
    assert_response :success
  end

end
