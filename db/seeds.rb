# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# :address, :gmaps, :latitude, :longitude
location = Location.new
location.address = "Ho Chi Minh city"
location.gmaps = true
location.latitude = "10.816400"
location.longitude = "106.644287"
location.save