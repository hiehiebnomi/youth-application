class AddGmapsToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :gmaps, :boolean
  end
end
