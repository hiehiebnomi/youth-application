class AddServiceToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :service, :string
  end
end
