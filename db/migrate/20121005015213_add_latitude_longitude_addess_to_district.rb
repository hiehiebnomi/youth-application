class AddLatitudeLongitudeAddessToDistrict < ActiveRecord::Migration
  def change
    add_column :districts, :latitude, :string
    add_column :districts, :longitude, :string
    add_column :districts, :address, :string
    add_column :districts, :latitude_late, :string
    add_column :districts, :longitude_late, :string
  end
end
