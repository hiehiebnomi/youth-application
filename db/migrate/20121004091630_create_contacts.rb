class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :address
      t.string :country
      t.string :district
      t.string :city
      t.string :ward
      t.string :province
      t.string :name
      t.string :img_default
      t.string :description

      t.timestamps
    end
  end
end
