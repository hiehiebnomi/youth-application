class AddLatitudeLongitudeAddessToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :latitude_late, :string
    add_column :contacts, :longitude_late, :string
  end
end
