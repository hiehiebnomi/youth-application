class AddGmailsToDistrict < ActiveRecord::Migration
  def change
    add_column :districts, :gmaps, :boolean
  end
end
